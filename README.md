# 5C | ProgramasDMM
## Desarrollo Móvil Multiplataforma VSCode 🌌📱
> Saldaña Espinoza Hector - Desarrollo de Software
>

### Contenido 🚀
- [ChatUtez](https://github.com/HectorSaldes/ProgramasDMM/tree/master/ChatUtez) - Simulación de un chat en linea con Firebase
- [ProyectoUtez](https://github.com/HectorSaldes/ProgramasDMM/tree/master/ProyectoUtez) - Haciendo uso de React Native CLI, app para cálcular intereses
- [mapas](https://github.com/HectorSaldes/ProgramasDMM/tree/master/mapas) - Uso de Google Maps
- [navegacion](https://github.com/HectorSaldes/ProgramasDMM/tree/master/navegacion) - Implementando el uso de navegaciones como Stak, Tabs y Drawer
- [peliutez](https://github.com/HectorSaldes/ProgramasDMM/tree/master/peliutez) - Aplicación haciendo uso de API y mostrando peliculas

import React, {useState, useMemo} from 'react';
import {View, Text, SafeAreaView, StatusBar} from 'react-native';

import {Provider as PaperProvider, DarkTheme as DarkThemePaper,  DefaultTheme as DefaultThemePaper,} from 'react-native-paper';
import { NavigationContainer, DarkTheme as DarkThemeNavigation,  DefaultTheme as DefaultThemeNavigation,} from '@react-navigation/native';
import AppNavigation from './src/navigation/AppNavigation';

import PreferencesContext from './src/context/PreferencesContext'

export default function App() {
  //console.log(DarkThemePaper);

  /* Cambiar los colores de la aplicacion */
  DefaultThemePaper.colors.primary = '#1ae1f2';
  DarkThemePaper.colors.primary = '#1ae1f2'; // Opciones drawe
  DarkThemePaper.colors.accent = '#1ae1f2';

  DarkThemeNavigation.colors.background = '#192734'; // Screen
  DarkThemeNavigation.colors.card = '#15212b'; // Barra del titulo

  const [theme, setTheme] = useState('dark');
  const toggleTheme = () => {
    setTheme(theme === 'dark' ? 'light' : 'dark');
  };

  /* Solo se van actualizar cuando haya un cambio en su estado */
  const preference = useMemo(() => ({
    toggleTheme,
    theme
  }), [theme])

  return (
    <PreferencesContext.Provider value={preference}>
    <PaperProvider
      theme={theme === 'dark' ? DarkThemePaper : DefaultThemePaper}>
      <StatusBar barStyle={theme === 'dark' ? 'light-content' : 'dark-content'} />
      <NavigationContainer theme={theme === 'dark' ? DarkThemeNavigation : DefaultThemeNavigation}>
        <AppNavigation />
      </NavigationContainer>
    </PaperProvider>
    </PreferencesContext.Provider>
  );
}

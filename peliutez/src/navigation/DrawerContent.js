import React, {useState, useEffect} from 'react';
import {View, StyleSheet} from 'react-native';

import {DrawerContentScrollView} from '@react-navigation/drawer';
import {Drawer, Switch, TouchableRipple, Text} from 'react-native-paper';
import usePreferences from '../hooks/usePreference';

export default function DrawerContent(props) {
  const {navigation} = props;
  // useState y te la estuctura
  const [active, setActive] = useState('home');

  const {theme, toggleTheme} = usePreferences();

  const onChangeScreen = (screen) => {
    setActive(screen);
    navigation.navigate(screen);
  };

  return (
    /* Esto debe coincidir con el StackNavigation */
    <DrawerContentScrollView>
      <Drawer.Section>
        <Drawer.Item
          label="Inicio"
          active={active === 'home'}
          onPress={() => onChangeScreen('home')}
          icon="home"
        />

        <Drawer.Item
          label="Populares"
          active={active === 'popular'}
          onPress={() => onChangeScreen('popular')}
          icon="star"
        />

        <Drawer.Item
          label="Nuevas"
          active={active === 'news'}
          onPress={() => onChangeScreen('news')}
          icon="new-box"
        />
      </Drawer.Section>

      <Drawer.Section title="Opciones">
        <TouchableRipple>
          <View style={styles.preference}>
            <Text>Cambiar tema {theme ==='dark'?'Ligth 🌞 ':'Oscuro 🌚'} </Text>
            <Switch value={theme === 'dark'} onValueChange={toggleTheme} />
          </View>
        </TouchableRipple>
      </Drawer.Section>
    </DrawerContentScrollView>
  );
}

const styles = StyleSheet.create({
  preference: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
});

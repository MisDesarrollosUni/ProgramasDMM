import React from 'react';
import {View, Text} from 'react-native';
import {createDrawerNavigator} from '@react-navigation/drawer';
import StackNavigation from './StackNavigation';
import DrawerContent from './DrawerContent';

const Drawer = createDrawerNavigator();

export default function AppNavigation() {
  return (
    <Drawer.Navigator initialRouteName="app" drawerContent={(props)=><DrawerContent {...props}/> }>
      <Drawer.Screen name="app" component={StackNavigation}/>
    </Drawer.Navigator>
  );
}

import React, {useState, useEffect} from 'react';
import {View, Text, ScrollView, StyleSheet} from 'react-native';
import {
  getNewsMoviesApi,
  getAllGenresApi,
  getOneGenreMoviesApi,
} from '../api/movies';
import {Title} from 'react-native-paper';
import CarouselVertical from '../components/CarouselVertical';
import CarouselMulti from '../components/CarouselMulti';
import {map} from 'lodash';

export default function HomeScreen(props) {
  const {navigation} = props;
  const [newMovies, setNewMovies] = useState(null);
  const [genreList, setGenreList] = useState([]);
  const [genreSelected, setGenreSelected] = useState(28); //por que el primero en la API es ese
  const [genreMovies, setGenreMovies] = useState(null);

  //console.log(newMovies);
  useEffect(() => {
    getNewsMoviesApi().then((response) => {
      setNewMovies(response.results);
    });
  }, []);

  //console.log(genreList);
  useEffect(() => {
    getAllGenresApi().then((response) => {
      setGenreList(response.genres);
    });
  }, []);

  useEffect(() => {
    getOneGenreMoviesApi(genreSelected).then((response) => {
      setGenreMovies(response.results);
    });
  }, [genreSelected]);

  const onChangeGenre = (newGenreId) => {
    setGenreSelected(newGenreId);
  };

  return (
    <ScrollView>
      {newMovies && (
        <View style={styles.news}>
          <Title style={styles.newsTitle}>Nuevas Peliculas</Title>
          <CarouselVertical data={newMovies} navigation={navigation} />
        </View>
      )}

      <View style={styles.genres}>
        <Title style={styles.genreTitle}>Peliculas por género</Title>
        <ScrollView
          style={styles.genreList}
          horizontal
          showsHorizontalScrollIndicator={false}>
          {map(genreList, (genre) => (
            <Text
              key={genre.id} // Al dar click en uno, colocar un color que simule que esta presionado
              style={[
                styles.genre,
                {color: genre.id !== genreSelected ? '#8697a5' : '#070707'},
              ]}
              onPress={() => onChangeGenre(genre.id)}>
              {genre.name}
            </Text>
          ))}
        </ScrollView>
        {genreMovies &&(
          <CarouselMulti data={genreMovies} navigation={navigation} /> 
        )}
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  news: {
    marginVertical: 10,
  },
  newsTitle: {
    marginBottom: 15,
    marginHorizontal: 20,
    fontWeight: 'bold',
    fontSize: 22,
  },
  genres: {
    marginTop: 20,
    marginBottom: 50,
  },
  genreTitle: {
    marginHorizontal: 20,
    fontWeight: 'bold',
    fontSize: 22,
  },
  genreList: {
    marginTop: 5,
    marginBottom: 15,
    paddingHorizontal: 20,
    padding: 10,
  },
  genre:{
     marginRight:20,
     fontSize:18,
     backgroundColor: '#E9ECF5',
     borderRadius: 10,
     padding:10
  }
});

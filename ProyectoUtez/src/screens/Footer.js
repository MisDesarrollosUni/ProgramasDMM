import React, {Component} from 'react';
import {Button, Text, StyleSheet, View, TouchableOpacity} from 'react-native';
import colores from '../utils/colores';

export default class Footer extends Component {
  render() {

    const {calcular} = this.props

    return (
      <View style={styles.viewBoton}>
        <TouchableOpacity style={styles.boton} onPress={calcular}>
          <Text style={styles.texto}> CALCULAR </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewBoton: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    backgroundColor: colores.PRIMARY_COLOR,
    height: 100,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  texto: {
    fontWeight: 'bold',
    fontSize: 18,
    color: 'white',
    textAlign: 'center',
  },
  boton:{
      backgroundColor:colores.PRIMARY_COLOR_DARK,
      padding:16,
      borderRadius:20,
      width:'75%'
  }
});

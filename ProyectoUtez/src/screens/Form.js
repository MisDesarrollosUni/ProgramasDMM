import React, {Component} from 'react';
import {Picker, StyleSheet, View} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import colores from '../utils/colores';
import RNPickerSelect from 'react-native-picker-select';

export default class Form extends Component {
  render() {
    const {setCapital, setInteres, setMeses} = this.props;
    return (
      <View style={styles.formulario}>
        <View style={styles.campos}>
          <TextInput
            style={styles.campo}
            placeholder="Cantidad a pedir"
            keyboardType="numeric"
            onChange={(e) => {
              setCapital(e.nativeEvent.text);
            }}
          />
          <TextInput
            style={[styles.campo, styles.campoPorcentaje]}
            placeholder="Interés %"
            keyboardType="numeric"
            onChange={(e) => {
              setInteres(e.nativeEvent.text);
            }}
          />
        </View>

        <RNPickerSelect
          style={pickerStyle}
          onValueChange={value => { setMeses(value); }}
          placeholder={{
            label: 'Selecciona el plazo',
            value: 0,
          }}
          items={[
            {label: '3 meses', value: '3'},
            {label: '6 meses', value: '6'},
            {label: '12 meses', value: '12'},
            {label: '24 meses', value: '24'},
          ]}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  formulario: {
    position: 'absolute',
    bottom: 0,
    width: '90%',
    paddingHorizontal: 50,
    backgroundColor: colores.PRIMARY_COLOR_DARK,
    borderRadius: 30,
    height: 180,
    justifyContent: 'center',
  },
  campos: {
    flexDirection: 'row',
  },
  campo: {
    height: 50,
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: colores.PRIMARY_COLOR,
    borderRadius: 5,
    width: '50%',
    marginRight: 5,
    marginLeft: -5,
    marginBottom: 10,
    marginHorizontal: 20,
    color: 'black',
  },
  campoPorcentaje: {
    width: '40%',
    marginLeft: 5,
  },
});

const pickerStyle = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30,
    backgroundColor: 'white',
    marginLeft: -5,
    marginRight: -5,
  },
  inputAndroid: {
    width: '90%',
    fontSize: 16,
    paddingVertical: 18,
    paddingHorizontal: 10,
    borderWidth: 0.5,
    borderColor: 'grey',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30,
    backgroundColor: 'white',
  },
});

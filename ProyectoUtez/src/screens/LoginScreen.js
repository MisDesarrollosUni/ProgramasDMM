import React, {Component} from 'react';
import {Button, SafeAreaView, Text, View} from 'react-native';

export default class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      texto: 'Hola como estas',
    };
  }

  alertar = this.props.myFunction;

  render() {
    const {nombre, edad, color} = this.props.usuario
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#c5221f'}}>
        <View>
          <Text>Inicio de sesión {this.state.texto}</Text>
        </View>
        <Button
          title="Presioname chaval"
          onPress={() => this.alertar(nombre, edad, color)}></Button>
      </SafeAreaView>
    );
  }
}

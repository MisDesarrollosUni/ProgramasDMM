import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';

export default class Resultado extends Component {
  render() {
    const {capital, intereses, meses, total, errores} = this.props;
    return (
      <View style={styles.contenedor}>
        {total.pagoMes && (
          <View style={styles.resultado}>
            <Text style={styles.titulo}> RESUMEN </Text>
            <Datos titulo="Cantidad solicitada" valor={`$${capital}`} />
            <Datos titulo="Interés" valor={`${intereses}%`} />
            <Datos titulo="Plazos" valor={`${meses} meses`} />
            <Datos titulo="Pago Mensual" valor={`${total.pagoMes}`} />
            <Datos titulo="Total a Pagar" valor={`${total.pagoTotal}`} />
          </View>
        )}
        <View>
          <Text style={styles.error}>{errores}</Text>
        </View>
      </View>
    );

    function Datos(props) {
      const {titulo, valor} = props;
      return (
        <View style={styles.valores}>
          <Text>{titulo}</Text>
          <Text>{valor}</Text>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  contenedor: {
    marginHorizontal: 40,
  },
  error: {
    textAlign: 'center',
    color: 'red',
    fontWeight: 'bold',
    fontSize: 20,
  },
  resultado: {
    padding: 30,
  },
  titulo: {
    fontSize: 30,
    textAlign: 'center',
    fontWeight: 'bold',
    marginBottom: 30,
  },
  valores: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
});

import React, {Component} from 'react';
import {
  Button,
  SafeAreaView,
  Text,
  View,
  StyleSheet,
  StatusBar,
} from 'react-native';
import Footer from './src/screens/Footer';
import Form from './src/screens/Form';
import Resultado from './src/screens/Resultado';
import colores from './src/utils/colores';

//rcc te permite hacer un componente facil y rápido

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      capital: null,
      interes: null,
      meses: null,
      errores: null,
      total: {
        pagoMes: null,
        pagoTotal: null,
      },
    };
  }

  render() {
    const calcular = () => {
      limpiar()
      if (!this.state.capital) {
        this.setState({errores: 'Ingresa la cantidad que quieres solicitar'});
      } else if (!this.state.interes) {
        this.setState({errores: 'Ingresa el interés del prestamo'});
      } else if (!this.state.meses) {
        this.setState({errores: 'Ingresa los meses a pagar'});
      } else {
        const inte = this.state.interes / 100;
        const fee =
          this.state.capital /
          ((1 - Math.pow(inte + 1, -this.state.meses)) / inte);
        this.state.total.pagoMes = fee.toFixed(2);
        this.state.total.pagoTotal = (fee * this.state.meses).toFixed(2);
        console.log(this.state.total);
      }
    };

    const setCapital = (capital) => {
      this.state.capital = capital;
    };

    const setInteres = (interes) => {
      this.state.interes = interes;
    };

    const setMeses = (meses) => {
      this.state.meses = meses;
    };

    const limpiar = () => {
      this.setState({errores: null});
      this.state.total.pagoMes = null
      this.state.total.pagoTotal =null
    };

    return (
      <SafeAreaView style={{flex: 1}}>
        <StatusBar barStyle="light-content" />
        <View style={styles.head}>
          <View style={styles.fondo}></View>
          <Text style={styles.tituloApp}> Cotizardor para Préstamos </Text>
          <Form
            setCapital={setCapital}
            setInteres={setInteres}
            setMeses={setMeses}
          />
        </View>
        <Resultado
          capital={this.state.capital}
          intereses={this.state.interes}
          meses={this.state.meses}
          total={this.state.total}
          errores={this.state.errores}
        />
        <Footer calcular={calcular} />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  head: {
    // backgroundColor: colores.PRIMARY_COLOR,
    height: 290,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    alignItems: 'center',
    color: 'white',
  },
  tituloApp: {
    fontSize: 25,
    fontWeight: 'bold',
    color: 'white',
    marginTop: 15,
  },
  fondo: {
    backgroundColor: colores.PRIMARY_COLOR,
    width: '100%',
    height: 200,
    borderBottomRightRadius: 30,
    borderBottomLeftRadius: 30,
    position: 'absolute',
    zIndex: -1,
  },
});

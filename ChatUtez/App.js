import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Button, Container} from 'native-base';
import LoginScreen from './src/screens/LoginScreen';
import ChatScreen from './src/screens/ChatScreen';

export default function App() {
  const [username, setUsername] = useState(null);

  return (
    <Container style={styles.container}>
      {!username ? <LoginScreen setUsername={setUsername} /> : <ChatScreen userName={username}/>}
    </Container>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#16202b',
  },
});

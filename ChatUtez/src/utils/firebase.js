import firebase from 'firebase/app';

// Your web app's Firebase configuration
/* const firebaseConfig = {
  apiKey: 'AIzaSyCK9YXRWh5Ul0ZwbXL-iZYMMOFf4lm-Bpw',
  authDomain: 'chatutez.firebaseapp.com',
  databaseURL: 'https://chatutez-default-rtdb.firebaseio.com',
  projectId: 'chatutez',
  storageBucket: 'chatutez.appspot.com',
  messagingSenderId: '929914358994',
  appId: '1:929914358994:web:3eaa9f5b6f00fa5446b10b',
}; */

const firebaseConfig = {
  apiKey: 'AIzaSyDqk0kEd9YCeW7jo63Esc33W6ftwDVs6ZQ',
  authDomain: 'chatutez-c776a.firebaseapp.com',
  databaseURL: 'https://chatutez-c776a-default-rtdb.firebaseio.com',
  projectId: 'chatutez-c776a',
  storageBucket: 'chatutez-c776a.appspot.com',
  messagingSenderId: '681460583953',
  appId: '1:681460583953:web:aa7ea3a97e255e7e81abb0',
};

// Initialize Firebase
export default firebase.initializeApp(firebaseConfig);
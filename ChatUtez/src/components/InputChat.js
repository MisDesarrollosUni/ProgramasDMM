import React, {useState, useEffect} from 'react';
import {StyleSheet, View, TouchableOpacity} from 'react-native';
import {Item, Input as InputNB, Icon} from 'native-base';
import * as ImagePicker from 'react-native-image-picker';

export default function InputChat({sendMessage}) {
  const [message, setMessage] = useState('');
  const [image, setImage] = useState(null);

  //console.log(image)

  const onSubmit = () => {
    if (message.length > 0) {
      sendMessage(message, image);
      setMessage('');
    } else if (image !== null) {
      sendMessage(message, image);
      setImage(null);
      setMessage('');
    }
  };

  const selectImage = () => {
    const options = {
      maxWidth: 2000,
      maxHeight: 2000,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
      includeBase64: true,
    };
    ImagePicker.launchCamera(options, response => {
      //console.log('res '+response)
      const source = {base: response.base64};
      setImage(source);
      //console.log(source)
    });
  };

  return (
    <View style={styles.container}>
      <Item style={styles.itemInput}>
        <InputNB
          placeholder="Mensaje..."
          placeholderTextColor="gray"
          style={styles.input}
          onChange={e => setMessage(e.nativeEvent.text)}
          value={message}
        />
        <TouchableOpacity onPress={selectImage}>
          <Icon name="camera" style={styles.iconSend} type="FontAwesome" />
        </TouchableOpacity>
        <TouchableOpacity onPress={onSubmit}>
          <Icon name="send" style={styles.iconSend} type="MaterialIcons" />
        </TouchableOpacity>
      </Item>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#16202b',

    paddingHorizontal: 20,
  },
  itemInput: {
    borderColor: '#fff',
  },
  input: {
    color: '#fff',
  },
  iconSend: {
    color: '#fff',
    marginLeft: 10,
  },
});

import React, {useState, useEffect} from 'react';
import {StyleSheet, View, Text, TouchableOpacity, Image} from 'react-native';
import {Item, Input as InputNB, Icon} from 'native-base';
import letterColors from '../utils/letterColors';

export default function MessageChat({
  key,
  message: {text, time, userName, base},
  name,
}) {
  const itsMe = name === userName;
  const [bgColorLetter, setBgColorLetter] = useState(null);

  useEffect(() => {
    const char = userName.trim()[0].toUpperCase();
    const indexLetter = char.charCodeAt() - 65;
    //setBgColorLetter()
    setBgColorLetter(letterColors[indexLetter]);
  }, []);

  const conditionalStyle = {
    container: {
      justifyContent: itsMe ? 'flex-end' : 'flex-start',
    },
    viewMessage: {
      backgroundColor: itsMe ? '#f0f0f1' : '#4b86f9',
    },
    message: {
      color: itsMe ? '#000' : '#fff',
      textAlign: itsMe ? 'right' : 'left',
    },
  };

  return (
    <View style={[styles.container, conditionalStyle.container]}>
      {!itsMe && (
        <View
          style={[
            {backgroundColor: `rgb(${bgColorLetter})`},
            styles.letterView,
          ]}>
          <Text style={styles.letter}>{userName.charAt(0)}</Text>
        </View>
      )}

      <View style={[styles.viewMessage, conditionalStyle.viewMessage]}>
        {text === '' ? (
          <View>
            <Image style={styles.image} source={{uri:`data:image/jpeg;base64,${base}`}}/>
           <Text
              style={[styles.time, itsMe ? styles.timeLeft : styles.timeRight]}>
              {userName} - {time}
            </Text>
          </View>
        ) : (
          <View>
            <Text style={[styles.message, conditionalStyle.message]}>
              {text}
            </Text>
            <Text
              style={[styles.time, itsMe ? styles.timeLeft : styles.timeRight]}>
              {userName} - {time}
            </Text>
          </View>
        )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    margin: 5,
    alignItems: 'center',
  },
  viewMessage: {
    borderRadius: 10,
    minHeight: 35,
    minWidth: '40%',
    maxWidth: '80%',
    backgroundColor: 'white',
  },
  message: {
    padding: 5,
    paddingBottom: 25,
  },
  time: {
    fontSize: 10,
    position: 'absolute',
    bottom: 5,
  },
  letterView: {
    height: 35,
    width: 35,
    borderRadius: 50,
    alignItems: 'center',
    marginRight: 10,
  },
  letter: {
    fontSize: 18,
    color: 'white',
    fontWeight: 'bold',
  },
  timeRight: {
    right: 8,
    color: 'white',
  },
  timeLeft: {
    left: 8,
    color: 'gray',
  },
  image:{
    width: 300,
    height: 250,
    resizeMode: 'stretch',
  }
});

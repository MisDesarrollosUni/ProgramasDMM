import React, {useEffect, useState, useRef} from 'react';
import {ScrollView, StyleSheet, View} from 'react-native';
import {Header, Body, Title, Text} from 'native-base';
import InputChat from '../components/InputChat';
import MessageChat from '../components/MessageChat';
import firebase from '../utils/firebase';
import 'firebase/database';
import moment from 'moment';
import { map } from 'lodash';

export default function ChatScreen({userName}) {

  const [messages, setMessages] = useState([])

  const sendMessage = (message, image) => {
   const time = moment().format('hh:mm a')
    firebase.database()
      .ref('general') // A que colección
      .push({ // Clave - valor, tengan el mismo nombre
      userName,
      text: message,
      time, 
      ...image
    })
  };

  useEffect(() => {
      const chat = firebase.database().ref('general')
      chat.on('value', snapshot=>{
          //console.log(snapshot.val())
          setMessages(snapshot.val());
      })
  }, [])

  const chatScrollRef = useRef();
  useEffect(() => {
      chatScrollRef.current.scrollTo({y: 100000})
  }, [messages])
  
  return (
    <>
      <Header style={styles.header}>
        <Body>
          <Title>Chat UTEZ</Title>
        </Body>
      </Header>
      <View style={styles.content}>
        <ScrollView style={styles.chatView} ref={chatScrollRef}>
          {
            map(messages, (message, index)=> (
              <MessageChat key={index} message={message} name={userName}/>
            ))
          }
        </ScrollView>
        <InputChat sendMessage={sendMessage} />
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#16202b',
  },
  content: {
    flex: 1,
    justifyContent: 'space-between',
  },
  chatView: {
    backgroundColor: '#1b2734',
  },
});

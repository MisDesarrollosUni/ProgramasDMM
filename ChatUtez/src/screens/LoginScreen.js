import React, {useState, useEffect} from 'react';
import {StyleSheet, View, SafeAreaView, StatusBar, Image} from 'react-native';
import {Item, Input, Text, Button} from 'native-base';
import Chat from '../assets/Chat.png';

export default function LoginScreen(props) {
  const {setUsername} = props;
  const [name, setName] = useState('');

  const onSumbit = () => {
    setUsername(name);
  };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="light-content" />
      <View>
        <Image source={Chat} resizeMode="contain" style={styles.logo} />
      </View>
      <Item>
        <Input
          placeholder="Nombre de un usuario"
          style={{color: '#fff'}}
          placeholderTextColor="gray"
          value={name}
          onChange={e => setName(e.nativeEvent.text)}
        />
      </Item>
      <Button style={styles.btnLogin} onPress={onSumbit}>
        <Text>Entrar</Text>
      </Button>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 50,
    marginVertical: 100,
  },
  logo: {
    width: '100%',
    height: 200,
    marginBottom: 30,
  },
  btnLogin: {
    marginTop: 40,
    justifyContent: 'center',
    backgroundColor: '#0098d3',
    width: '100%',
  },
});

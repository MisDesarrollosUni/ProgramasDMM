import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import HomeScreen from '../screens/HomeScreen';
import AboutScreen from '../screens/AboutScreen';
import ContactScreen from '../screens/ContactScreen';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import HomeStack from './HomeStack';
import ContactStack from './ContactStack';

import {createDrawerNavigator} from '@react-navigation/drawer';

const Drawer = createDrawerNavigator();

export default function AppNavigation() {
  return (
    <Drawer.Navigator>
      <Drawer.Screen
        name="home" // en vez de traer el componente screen, traemos un componente que contiene un stack
        component={HomeStack}
        options={{title: 'Home'}}
      />
      <Drawer.Screen
        name="about"
        component={AboutScreen}
        options={{title: 'About'}}
      />
      <Drawer.Screen
        name="contact"
        component={ContactStack}
        options={{title: 'Contact'}}
      />
    </Drawer.Navigator>
  );
}

//  USANDO TABS

/* const Tab = createBottomTabNavigator();
export default function AppNavigation() {
  return (
    <Tab.Navigator>
      <Tab.Screen
        name="home" // en vez de traer el componente screen, traemos un componente que contiene un stack
        component={HomeStack}
        options={{title: 'Home'}}
      />

      <Tab.Screen
        name="about"
        component={AboutScreen}
        options={{title: 'About'}}
      />

      <Tab.Screen
        name="contact"
        component={ContactStack}
        options={{title: 'Contact'}}
      />
    </Tab.Navigator>
  );
} */


// ESTA FORMA ES USANDO SOLO STACK

/* const Stack = createStackNavigator();
export default function AppNavigation() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="home"
        component={HomeScreen}
        options={{title: 'Home'}}
      />
      <Stack.Screen
        name="about"
        component={AboutScreen}
        options={{title: 'About'}}
      />
      <Stack.Screen
        name="contact"
        component={ContactScreen}
        options={{title: 'Contact'}}
      />
    </Stack.Navigator>
  )
}
 */

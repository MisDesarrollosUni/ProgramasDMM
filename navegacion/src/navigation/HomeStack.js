import React from 'react';
import {View, Text} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';

import HomeScreen from '../screens/HomeScreen';
import AboutScreen from '../screens/AboutScreen';
import ContactScreen from '../screens/ContactScreen';

const Stack = createStackNavigator();

export default function HomeStack(props) {
  const {navigation} = props;
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="home"
        component={HomeScreen}
        options={{
          title: 'Home',
          headerLeft: () => <Text onPress={navigation.openDrawer}>    MENÚ</Text>,
        }}
      />

      {/*  <Stack.Screen
        name="about"
        component={AboutScreen}
        options={{title: 'About'}}
      />

      <Stack.Screen
        name="contact"
        component={ContactScreen}
        options={{title: 'Contact'}}
      /> */}
    </Stack.Navigator>
  );
}

import React from 'react';
import {View, Text} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';

import HomeScreen from '../screens/HomeScreen';
import AboutScreen from '../screens/AboutScreen';
import ContactScreen from '../screens/ContactScreen';
import CoursesScreen from '../screens/CoursesScreen';

const Stack = createStackNavigator();

export default function ContactStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="contact"
        component={ContactScreen}
        options={{title: 'Contact'}}
      />

      <Stack.Screen
        name="courses"
        component={CoursesScreen}
        options={{title: 'Courses'}}
      />

      {/*  <Stack.Screen
        name="home"
        component={HomeScreen}
        options={{title: 'Home'}}
      />
      <Stack.Screen
        name="about"
        component={AboutScreen}
        options={{title: 'About'}}
      /> */}
    </Stack.Navigator>
  );
}

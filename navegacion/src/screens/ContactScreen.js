import React from 'react';
import {Text, View, Button} from 'react-native';

export default function ContactScreen(props) {
  const {navigation} = props;
  return (
    <View>
      <Text>Hola desde Contact Screen</Text>
      <Button title="Ir a Home" onPress={() => navigation.navigate('home')} />
      <Button title="Ir a About" onPress={() => navigation.navigate('about')} />
    </View>
  );
}

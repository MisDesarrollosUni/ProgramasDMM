import React from 'react';
import {Text, View, Button} from 'react-native';

export default function AboutScreen(props) {
  const {navigation} = props;
  return (
    <View>
      <Text> Hola desde About Screen </Text>
      <Button title="Ir a Home" onPress={() => navigation.navigate('home')} />
      <Button title="Ir a Contact" onPress={()=> navigation.navigate('contact')} />
    </View>
  );
}

import React from 'react';
import {Button, Text, View} from 'react-native';
// rfc - crear un componente funcional
export default function HomeScreen(props) {
  const {navigation} = props;
  return (
    <View>
      <Text> Hola desde Home Screen </Text>
      <Button title="Ir a About" onPress={() => navigation.navigate('about')} />
      <Button title="Ir a Contact" onPress={()=> navigation.navigate('contact')} /> 
                                                             {/*  // Asi se usar el stack de otra pantalla */}
      <Button title="Ir a Courses" onPress={()=> navigation.navigate('contact', {screen: 'courses'})} />
    </View>
  ); //85
}

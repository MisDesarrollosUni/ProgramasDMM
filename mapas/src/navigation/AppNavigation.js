import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import GeoScreen from '../screens/GeoScreen';
import MapScreen from '../screens/MapScreen';

const Stack = createStackNavigator();

export default function AppNavigation() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="home"
        component={GeoScreen}
        options={{
          title: 'Mi Geolocalización',
        }}
      />
      <Stack.Screen
        name="map"
        component={MapScreen}
        options={{
          title: 'Mi Mapa',
        }}
      />
    </Stack.Navigator>
  );
}
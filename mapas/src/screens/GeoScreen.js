import React, {useState} from 'react';
import {AsyncStorage, Button, StyleSheet, Text, View} from 'react-native';
import RNLocation from 'react-native-location';

RNLocation.configure({
  distanceFilter: null,
});

export default function GeoScreen(props) {
  const {navigation} = props;
  const [viewLocation, setViewLocation] = useState({
    latitude:'',
    longitude:'',
  });

  const guardarDatos = async ()=>{
    try{
      await AsyncStorage.setItem('@guardar_nombre', 'Hector Saldaña')
      alert('Se guardo el nombre')
    }catch(e){
      alert('Falló guardar nombre');
      console.error(e);
    }
  }

  const goMapa = () => {
    console.log(viewLocation);
    navigation.navigate('map', {
      lat: viewLocation.latitude,
      lon: viewLocation.longitude,
    });
  };

  const permisionHandler = async () => {
    let permission = await RNLocation.checkPermission({
      ios: 'whenInUse',
      android: {
        detail: 'coarse',
      },
    });
    console.log(permission);
    let location;
    if (!permission) {
      permission = await RNLocation.checkPermission({
        ios: 'whenInUse',
        android: {
          detail: 'coarse',
          rationale: {
            title: 'Necesitamos acceder a tu ubicación',
            message: 'Vamos a utilizar tu ubicación para pintarte en el mapa',
            buttonPositive: 'Aceptar',
            buttonNegative: 'Cancelar',
          },
        },
      });
      console.log(permission);
      location = await RNLocation.getLatestLocation({timeout: 100});
      console.log(
        location,
        location.longitude,
        location.latitude,
        location.timestamp,
      );
      setViewLocation(location);
      guardarDatos()
    } else {
      location = await RNLocation.getLatestLocation({timeout: 100});
      console.log(
        'Location: ',
        location,
        'Longitud: ',
        location.longitude,
        'Longitude: ',
        location.latitude,
        'Timestamp: ',
        location.timestamp,
      );
      setViewLocation(location);
      guardarDatos()
    }
  };

  return (
    <View style={styles.container}>
      <Text style={{fontWeight: 'bold', fontSize: 25}}>
        Bienvenido a GeoScreen
      </Text>
      <View style={styles.geo}>
        <Button title="Obtener mi ubicación" onPress={permisionHandler} />
      </View>
      <Text>Latitud: {viewLocation.latitude}</Text>
      <Text>Longitud: {viewLocation.longitude}</Text>
      <View style={styles.geo}>
        <Button title="Ver coordenadas en mapa" onPress={goMapa} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  geo: {
    marginTop: 10,
    padding: 10,
    borderRadius: 10,
    width: '40%',
  },
});

import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, AsyncStorage} from 'react-native';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import {Circle} from 'react-native-maps';
import {Marker} from 'react-native-maps';

export default function GeoScreen(props) {
  //console.log(props.route.params)
  const {lat, lon} = props.route.params;
  console.log(lat, lon);

  const [name, setName] = useState(null);

  const leerDato = async () => {
    try {
      const username = await AsyncStorage.getItem('@guardar_nombre');
      if (username !== null) {
        setName(username);
      }
    } catch (e) {
      alert('No se pudo obtener el dato');
    }
  };

  useEffect(() => {
    leerDato();
  }, []);

  return (
    <View style={styles.container}>
      <Text style={{fontWeight: 'bold', fontSize: 20}}>
        Bienvenido a MapScreen {name}
      </Text>
      <View style={styles.map}>
        <MapView
          provider={PROVIDER_GOOGLE}
          showsUserLocation={true}
          style={{width: '100%', height: '100%'}}
          region={{
            latitude: lat,
            longitude: lon,
            latitudeDelta: 0.004,
            longitudeDelta: 0.004,
          }}>
          <Marker
            showUserLocation={true}
            coordinate={{
              latitude: lat,
              longitude: lon,
            }}
            title="Mi marcador"
            description="Este es mi primer marcador"
          />

          <Circle
            showsUserLocation={true}
            center={{
              latitude: lat,
              longitude: lon,
            }}
            radius={50}
            fillColor={'rgba(255,0,0,0.2)'}
            strokeColor={'red'}
            strokeWidth={2}
          />
        </MapView>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 20,
    marginBottom: 20,
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  map: {
    marginTop: 10,
    padding: 10,
    borderRadius: 10,
    width: '100%',
  },
});
